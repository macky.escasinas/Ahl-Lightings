import '../App.css';
import { Link } from "react-router-dom";
import {Navbar, Nav, Container, NavDropdown,Form,FormControl,Button,Image,Card,Col,Row} from "react-bootstrap";
import React, { useState ,useContext,useEffect} from 'react';
import UserContext from "../UserContext";
import ShopCart from "../components/ShopCart";
import Swal from "sweetalert2";
import Offcanvas from 'react-bootstrap/Offcanvas';
import { FaShoppingCart,FaUserPlus,FaWindowClose,FaHome,FaRegRegistered,FaStore,FaUserAlt,FaSignOutAlt } from "react-icons/fa";
import { AiFillDashboard } from "react-icons/ai";

import { ImHome } from "react-icons/im";

import Table from 'react-bootstrap/Table';
import { Navigate } from "react-router-dom";

export default function AppNavBar() {
  const {user} = useContext(UserContext);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [totalAmount, setTotalAmount] = useState(0);
  const [orderId, setOrderId] = useState();
  const [productsCart, setProductCart] = useState([]);
  const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1
        }}
    />
);
  
  const checkOut = () =>{
    const token = localStorage.getItem('token');
    if(token==null){
        Swal.fire({
          title: "Please Login first",
          icon: "error",
          text: "Login/Register first before you can checkOut"
        })
    }else{
            const listOfProductOrder = JSON.parse(localStorage.getItem('productList'));
           fetch(`${process.env.REACT_APP_API_URL}/orders/create-order`, {
                 method: "POST",
                 headers:{
                   "Content-Type": "application/json",
                   "Authorization": `Bearer ${localStorage.getItem('token')}`
                 }
               })
               .then(res => res.json())
               .then(data => {  
                 setOrderId(data._id);

                 listOfProductOrder.map(orderList =>{
                  
                     const qtyProductOrder = JSON.parse(localStorage.getItem(orderList));


                   fetch(`${process.env.REACT_APP_API_URL}/orders/${data._id}/add-product`, {
                           method: "POST",
                           headers:{
                             "Content-Type": "application/json",
                             "Authorization": `Bearer ${localStorage.getItem('token')}`
                           },
                              body: JSON.stringify({
                                   productId:orderList,
                                   quantity:qtyProductOrder
                       })
                         })
                         .then(res => res.json())
                         .then(data2 => {
                          console.log(data2);
                         
                          if(data2){

                          let timerInterval
                          Swal.fire({
                           title: "Order Created",
                           icon: "success",
                           html: 'Generating CheckOut',
                            timer: 5000,
                            timerProgressBar: true,
                            didOpen: () => {
                              Swal.showLoading()
                              const b = Swal.getHtmlContainer().querySelector('b')
                              timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                              }, 100)
                            },
                            willClose: () => {
                              clearInterval(timerInterval)
                            }
                          }).then((result) => {

                            if (result.dismiss === Swal.DismissReason.timer) {
                              //window.location.href = `/checkout/${data._id}`;
                            }
                          })


                            

                          }else{
                            Swal.fire({
                               title: "Order Not create",
                            icon: "error",
                           
                            })

                          }
                            
                         localStorage.removeItem(orderList);
                     
                         
                         })
                           

                        localStorage.removeItem("productList");   
                 })
                  

       })
    }

         
      
}

const viewCheck=()=>{
     let amountTotal=0;

    if(JSON.parse(localStorage.getItem('productList')!=null)){


        const listOfProduct = JSON.parse(localStorage.getItem('productList'));
      
        setProductCart(listOfProduct.map(productList =>{
         

           const qtyProduct = JSON.parse(localStorage.getItem(productList));
            fetch(`${process.env.REACT_APP_API_URL}/products/${productList}`)
            .then(res => res.json())
            .then(data => { 
              amountTotal=amountTotal+(qtyProduct*data.price);
              setTotalAmount(amountTotal);   
           })
            return(
                  <>
                  <Col  lg={1}><FaWindowClose/></Col>
                    <ShopCart key={productList} productProp={productList} />
                      <ColoredLine color="black" />
                  </>
                ) 

        }))
    }
       
       handleShow();
}

  useEffect(() =>{
    
       
    },[])
  const Footer = () => {
    return(
        <div className="social-wrap">
            <h4>Socia Networks Footer</h4>
            <ul className="social">
                <li>
                    <a href="facebook">Facebook</a>
                </li>
                <li>
                    <a href="twitter">Twitter</a>
                </li>
            </ul>
        </div>
    )
}
  return (
    <>
    <Offcanvas show={show}   onHide={handleClose} responsive="lg" placement="end">
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Cart</Offcanvas.Title>

        </Offcanvas.Header>
        <ColoredLine color="black" />
        <Offcanvas.Body>
     {/*     <FaShoppingCart/>*/}
        {productsCart}
   
           
        </Offcanvas.Body>

         <Card >

              <Card.Body>
                        <Card.Subtitle>CALCULATED AT CHECKOUT</Card.Subtitle>
                        <Card.Text></Card.Text>
                        <Card.Subtitle>Total: </Card.Subtitle>
                        <Card.Text>{totalAmount}</Card.Text>                 
                        <Button  variant="success" className="justifyContent" onClick ={() => checkOut()}>CHECKOUT →</Button>
              </Card.Body>
    </Card>
      </Offcanvas>

<Row className="justify-content-md-center">

<Col ></Col>

<Col>
     <p className="text-center"><img src="../logo.jpg" alt="" width="250" height="100" />
   
   </p>
</Col>
<Col> 
    
    </Col>
</Row>

    
      
    
 
     <Navbar  expand="lg">

  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav" className="justify-content-center" >
    <Nav >
      <Nav.Link as={Link} to="/home" eventKey="/home"><h5 className="navFont" ><FaHome /> HOME</h5></Nav.Link>
        <Nav.Link as={Link} to="/product" eventKey="/product"><h5 className="navFont"><FaStore /> PRODUCT</h5></Nav.Link>
   
         

         {




                (user.id !== null)
                ?  <>
                  

                  </> 
                  
                :
                  <>
                    <Nav.Link as={Link} to="/register" eventKey="/register"><h5 className="navFont"><FaRegRegistered /> REGISTER</h5></Nav.Link>

                    <Nav.Link as={Link} to="/login" eventKey="/login"><h5 className="navFont"><FaUserAlt /> LOGIN</h5></Nav.Link>

                  </> 
                  
        }

        {
         
          (user.isAdmin)
          ?
            
            <Nav.Link as={Link} to="/admin" eventKey="/admin"><h5 className="navFont"><AiFillDashboard /> DASHBOARD</h5></Nav.Link>
          
            
          :
        
            
            <Nav.Link onClick={viewCheck}><h5 className="navFont"><FaShoppingCart size={25}/> Cart</h5></Nav.Link>
           
          
        }
         {
           (user.id !== null)
           ?
           <><Nav.Link as={Link} to="/logout" eventKey="/logout" ><h5 className="navFont"><FaSignOutAlt />  LOGOUT</h5></Nav.Link></> 
           :
           <></> 

        }
        
       


    </Nav>
   
  </Navbar.Collapse>
</Navbar>
    </>
  );
}

