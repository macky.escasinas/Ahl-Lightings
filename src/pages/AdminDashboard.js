import React,{ useContext, useState, useEffect } from "react";
import {Table, Button,Modal,Form,Row,Col} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";
import Axios from "axios";
import Swal from "sweetalert2";
import Image from "react-bootstrap/Image";
import {useRef} from 'react';
import {
  DatatableWrapper,
  Filter,
  Pagination,
  PaginationOptions,
  TableBody,
  TableHeader
} from "react-bs-datatable";

export default function AdminDashboard(){
	const tableBody=[];
	  	const {user} = useContext(UserContext);
	  	const [show, setShow] = useState(false);
	  	const handleClose = () => setShow(false);
	  	const handleShow = () => setShow(true);

	  	const [showAllOrder, setShowAllOrder] = useState(false);
	  	const orderClose = () => setShowAllOrder(false);
	  	const orderShow = () => setShowAllOrder(true);
		const [orderList, setOrderList] = useState([]);
			const [name, setName] = useState();
			const [description, setDescription] = useState();
			const [price, setPrice] = useState();
			const [stock, setStock] = useState();
  		const [file, setFile] = useState();
			const [allProducts, setAllProducts] = useState([]);
		 const [buttonSave, setButtonSave] = useState("");
		 const [productId, setProductId] = useState("");
		 const [productImg, setProductImage] = useState("");
	const STORY_HEADERS = [
	  {
	    prop: "_id",
	    title: "Order Id",
	    isFilterable: true
	  },
	  {
	    prop: "userId",
	    title: "User Id"
	  },
	  {
	    prop: "totalAmount",
	    title: "Total Amount"
	  },
	  {
	    prop: "purchaseOn",
	    title: "Purchase On"
	  },
	  {
	    prop: "status",
	    title: "Status",
	    isSortable: true
	  }
	];
		 const addNewProduct = () => {
		 		setButtonSave("Add Product");
		 		setName("");
      	setDescription("");
      	setPrice("");
      	setStock("");
      	setFile("");
		 		handleShow();
		 }
		  const showAllOrders = () => {
				fetch(`${process.env.REACT_APP_API_URL}/orders/all-order`,{
					headers:{
						"Authorization": `Bearer ${localStorage.getItem("token")}`
					}
				})
				.then(res => res.json())
				.then(data => {
								setOrderList(data);			

				})	
		 		orderShow();
		 }

		 const saveProduct=()=>{
		 		if(buttonSave=="Add Product"){	
		 			addProduct();
		 		}else{
		 			saveChanges();
		 		}

		 }
const saveChanges = async (event) => {

	let url=``;

    	const productData = new FormData();
    		productData.append("name", name);
   		 	productData.append("description", description);
    		productData.append("price", price);
    		productData.append("stock", stock);
    		
    		if(file=="undefined"){
    			 url=`${process.env.REACT_APP_API_URL}/products/images/${productId}`
    		}else{
    			productData.append("file", file);
    			 url=`${process.env.REACT_APP_API_URL}/products/${productId}`
    		}
   		 Axios.put(url, productData,{
      headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
      .then(res =>{
      	if(productData){
      		Swal.fire({
      			icon: 'success',
      			title: 'Product Updated!'
      		})
      	}else{
      		Swal.fire({
					title: "Add Product Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again!`
			})

      	}

      	setName("");
      	setDescription("");
      	setPrice("");
      	setStock("");
      	setFile("");
				handleClose();
				fetchData();
      })
      .catch(err => 
      	console.log(err)
	    //   	Swal.fire({

					// 	title: "Add Product Unsuccessful!",
					// 	icon: "error",
					// 	text: `Please Complete the details. Please try again!`
					// })

			);
    
  };
  
const addProduct = async (event) => {
    	const data = new FormData();
    data.append("name", name);
    data.append("description", description);
    data.append("price", price);
    data.append("stock", stock);
    data.append("file", file);
   	if(file=="" || description==""|| price==""|| stock==""){
   		      Swal.fire({
   							title: "Add Product Unsuccessful!",
   							icon: "error",
   							text: `Please Complete details. Please try again!`
   					})
   	}else{



    Axios.post(`${process.env.REACT_APP_API_URL}/products/create-product`, data,{
      headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
      .then(res =>{
      	if(data){
      		Swal.fire({
      			icon: 'success',
      			title: 'Product Save!'
      		})
      	}else{
      		Swal.fire({
					title: "Add Product Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again!`
			})

      	}

      	setName("");
      	setDescription("");
      	setPrice("");
      	setStock("");
      	setFile("");
				handleClose();
				fetchData();
      })
      .catch(err => Swal.fire({
					title: "Add Product Unsuccessful!",
					icon: "error",
					text: `Please Complete the details. Please try again!`
			}));
		}
    
  };
  

  	const editProduct = (productId) =>{
  		setProductId(productId);
  		setButtonSave("Save Changes");
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			

      	setName(data.name);
      	setDescription(data.description);
      	setPrice(data.price);
      	setStock(data.stock);
      	setProductImage(data.productImage);


      	handleShow();
		})
	}



	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/viewall-product`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			tableBody.push(data);
	


			setAllProducts(data.map(product => {
				return(




					<tr key={product._id}>
						<td>{product._id}</td>
						<td className="text-center"><Image src={`${process.env.REACT_APP_API_URL}/${product.productImage}`}  alt="" width="150" height="150"roundedCircle /></td>
					
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stock}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" onClick ={() => editProduct(product._id)} >Edit</Button>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
		console.log()
	}

	const archive = (productId, productName) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{


			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	const unarchive = (productId, productName) =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			//console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	useEffect(()=>{
	
		fetchData();
	}, [])


  
	return(
		(user.isAdmin)
		?
		<>	

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Product Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" >
              <Form.Label>Name</Form.Label>
           
              <Form.Control type="text" id="name" value={name} onChange={event => { const { value } = event.target;setName(value);}} />
            </Form.Group>
            <Form.Group className="mb-3"  >
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} value={description} id="description" onChange={event => { const { value } = event.target;setDescription(value);}}/>
            </Form.Group>
            <Form.Group className="mb-3" >
              <Form.Label>Price</Form.Label>
              <Form.Control type="number" id="price" value={price} onChange={event => { const { value } = event.target;setPrice(value);}} />
            </Form.Group>
            <Form.Group className="mb-3" >
              <Form.Label>Stocks</Form.Label>
              <Form.Control type="number" id="stocks"  value={stock} onChange={event => { const { value } = event.target;setStock(value);}} />
            </Form.Group>

            <Form.Group  className="mb-3">
        	<Form.Control type="file"   id="file" accept=".jpg"  file={file} onChange={event => { const file = event.target.files[0];setFile(file);
              }}/>
      		</Form.Group>

          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary"  onClick={saveProduct} className="mx-2">
            {buttonSave}
          </Button>
        </Modal.Footer>
      </Modal>


<Modal show={showAllOrder} onHide={orderClose} size="xl"
      aria-labelledby="contained-modal-title-vcenter"
      centered> 
        <Modal.Header closeButton>
          <Modal.Title>Order List</Modal.Title>
        </Modal.Header>
        <Modal.Body>
         		
		      	
		     <DatatableWrapper
      body={orderList}
      headers={STORY_HEADERS}
      paginationOptionsProps={{
        initialState: {
          rowsPerPage: 10,
          options: [5, 10, 15, 20]
        }
      }}
    >
      <Row className="mb-4 p-2">
        <Col
          xs={12}
          lg={4}
          className="d-flex flex-col justify-content-end align-items-end"
        >
          <Filter />
        </Col>
        <Col
          xs={12}
          sm={6}
          lg={4}
          className="d-flex flex-col justify-content-lg-center align-items-center justify-content-sm-start mb-2 mb-sm-0"
        >
          <PaginationOptions />
        </Col>
        <Col
          xs={12}
          sm={6}
          lg={4}
          className="d-flex flex-col justify-content-end align-items-end"
        >
          <Pagination />
        </Col>
      </Row>
      <Table>
        <TableHeader />
        <TableBody />
      </Table>
    </DatatableWrapper>

		 
        </Modal.Body>
        <Modal.Footer>
        

          <Button variant="secondary" onClick={orderClose}>
            Close
          </Button>
         
        </Modal.Footer>
      </Modal>


			<div className="mt-5 mb-3 text-center">

				<h1>Admin Dashboard</h1>
	
				<Button variant="primary" size="lg" onClick={addNewProduct} className="mx-2">Add Product</Button>
				<Button variant="success" size="lg" onClick={showAllOrders} className="mx-2">Show All Orders</Button>
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Product ID</th>
		          <th>Image</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Stock</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>





		</>

		:
		<Navigate to="/product" />





	)
}
