import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Card, Button,Image,Table,Container,Col,Row,ButtonGroup,Form,InputGroup} from "react-bootstrap";

import ReactDOM from 'react-dom';
import CounterInput from 'react-bootstrap-counter';

import { FaRegPlusSquare,FaRegMinusSquare } from "react-icons/fa";
export default function ShopCart({productProp}){
     const [name, setName] = useState();
      const [description, setDescription] = useState();
      const [price, setPrice] = useState();

const [allProductsOrder, setAllProductsOrder] = useState([]);

const productId = productProp;

const [productImage, setProductImage] = useState();
const imagePath=`${process.env.REACT_APP_API_URL}/${productImage}`;

 const [orderQuantity, setOrderQuantity] = useState();

const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);

 const fetchProduct = () =>{
  fetch(`http://localhost:4000/products/${productId}`)
    .then(res => res.json())
    .then(data => { 
       const qtyProduct = JSON.parse(localStorage.getItem(productId));
      setName(data.name);
      setPrice(qtyProduct*data.price);
      setOrderQuantity(qtyProduct);
      setProductImage(data.productImage);
    })
 }


useEffect(()=>{
  fetchProduct();
    
    
  },)
const addOne = (productId) =>{
    const qty=parseInt(localStorage.getItem(productId))+1;
    localStorage.setItem(productId,qty);

    fetchProduct();


}
const minusOne = (productId) =>{
    const minusQty=parseInt(localStorage.getItem(productId))-1;
    localStorage.setItem(productId,minusQty);
    fetchProduct();


}



  return (
    <>

    <Container>
      <Row >
        <Col lg={4}><Image src={`${imagePath}`} style={{ width: '6rem'}} roundedCircle/></Col>
        <Col  lg={8}>
            <Row >{name} </Row >
            <Row >
             <Col lg={12}>

             <InputGroup>
           <Button onClick ={() => minusOne(productId)}><FaRegMinusSquare/></Button> 
          <Form.Control
           className="w-25"
           type="number"
           placeholder="Qty"
           value={orderQuantity} readOnly
          />
           <Button onClick ={() => addOne(productId)} ><FaRegPlusSquare/></Button> 
       
        </InputGroup></Col>
            
 <Col>Price:₱{price}</Col>
           </Row >
        </Col>
      </Row>
    </Container>

    {/*<Row xs={1} md={2} className="g-4">
      <Col lg={4}>
               <Row>{name}</Row>
               <Row></Row>
               <Row>{stock}</Row>
             </Col>
        
             <Col lg={8}>{price}</Col>
        <Col>
          <Card>
            <Card.Img variant="top" src="holder.js/100px160" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a longer card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
     
    </Row>*/}
    
                
    </>
  )
}
