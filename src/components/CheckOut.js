import { Link, useParams, useNavigate } from "react-router-dom";
import { Card, Button,Image,Table,Container,Col,Row,ButtonGroup,Form,InputGroup} from "react-bootstrap";
import React, { useState ,useContext,useEffect} from 'react';

import ProductCard from "../components/ProductCard";
export default function CheckOut(){

  
  const {orderId} = useParams();
const [total, setTotal] = useState(0);

const [tableList, setTableList] = useState();
const [orderList, setOrderList] = useState([]);
const [productLists, setProductLists] = useState([]);



//let orderList;

const viewList = () => {
       
         fetch(`${process.env.REACT_APP_API_URL}/orders/user-order/${orderId}`, {
                 method: "GET",
                 headers:{
                   "Content-Type": "application/json",
                   "Authorization": `Bearer ${localStorage.getItem('token')}`
                 }
               })
               .then(res => res.json())
               .then(data => {    
                   
                     setOrderList(data[0].products); 
                     setTotal(data[0].totalAmount);  
                    
                  

         
                })

               

        

}
 useEffect(() =>{
      viewList(); 
     /* if(orderList.length!==0){
              setTableList(orderList.map(product => {
               

                  

               

                return (
                     <>

                     <tr key={product.id} >
                     <td> </td>
                     <td> </td>
                     <td>{product.quantity}</td>
                     <td>{product.subTotal}</td>
                     </tr>
                     </>
                  )
                  


             
            



              

          }))

          

      }*/
console.log(orderList);
     
    },[total])
        




return (
		<>	
		 
			
		<Row className="justify-content-center">
		 
			 <Col lg="6">
				
				 <Card className="text-center mt-3" variant="primary">
      <Card.Header><h4>Order List</h4></Card.Header>
      <Card.Body>
      <Table striped bordered hover>
      <thead>
        <tr>
          <th>Product</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Sub Total</th>
        </tr>
      </thead>
      <tbody>
      

       

      
       
        
        {
          tableList

        }
        <tr>
          <td colSpan="3">Total</td>
          <td>{total}</td>
         
        
         
        </tr>
      </tbody>
    </Table>
        
      </Card.Body>
   
    </Card>
				

			</Col>
		</Row>

			




		</>
		)
}