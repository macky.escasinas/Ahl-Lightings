import { useContext } from "react";
import { Link } from "react-router-dom";
import { Card, Button} from "react-bootstrap";
import Image from "react-bootstrap/Image";

import {FaCartPlus} from "react-icons/fa";
import Col from 'react-bootstrap/Col';

import Swal from "sweetalert2";
import UserContext from "../UserContext";

//Deconstruct the "courseProp" form the "props" object to shorten syntax.
export default function ProductCard({productProp}){
	  	const {user} = useContext(UserContext);
let productLists=[];
	
	const {_id, name, description, price, stock,productImage} = productProp;
	const imagePath=`${process.env.REACT_APP_API_URL}/${productImage}`;
 const addThisToCart = (productId) =>{


 		const list=JSON.parse(localStorage.getItem('productList'));
			
		if(list!=null){
			productLists=list;
			if(productLists.indexOf(productId)===-1){
				productLists.push(productId);
				localStorage.setItem(productId, 1);
				Swal.fire({
                               title: "Product Added",
                            icon: "success",
                             text: "You can change quantity in Cart"
                  })

			}else{
				 Swal.fire({
                               title: "Product Aldeady Added",
                            icon: "error",
                             text: "You can change quantity in Cart"
                  })
			}
			localStorage.setItem('productList', JSON.stringify(productLists));
			

		

		}else{
			
			productLists.push(productId);
			localStorage.setItem('productList', JSON.stringify(productLists));	
			localStorage.setItem(productId, 1);	
			Swal.fire({
                               title: "Product Added",
                            icon: "success",
                             text: "You can change quantity in Cart"
                  })


		}

	
 }


	return (
		<>

	
      <Col xs lg="4">

		 <Card className="p-3 my-3 text-center" >
      		
      <Card.Body>
      <Image src={`${imagePath}`} roundedCircle className="justify-content-center"   width={250} height={250}/>
	   						        <Card.Title>
	   						            {name}
	   						        </Card.Title>
	   						        <Card.Subtitle >Description: </Card.Subtitle>
	   						        <Card.Text >
	   						            {description}
	   						        </Card.Text>
	   						        <Card.Subtitle>Price: </Card.Subtitle>
	   						        <Card.Text>
	   						            ₱ {price.toLocaleString()}
	   						        </Card.Text>
	   						        <Card.Text>
	   						            Stocks: {stock}
	   						        </Card.Text>
	   						    	
	   						   
	   						        	{
	   						        		(user.isAdmin)
													?
													     <Button as={Link} to={`/product/${_id}`} variant="primary">Details</Button>
													:
															<> 
																		



															<Button as={Link} to={`/product/${_id}`} variant="primary">Details</Button> 
															<Button  variant="success" className="justifyContent"  onClick ={() => addThisToCart(_id)}><FaCartPlus/> Cart →</Button>
															</>
	   						        	}

	   						         
	   						     
	   						    </Card.Body>
    </Card>
	   	 </Col>
		</>
	)
}
