import {useState, useEffect, useContext} from "react";

import {Navigate, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import {Button, Form,Row,Col,Card} from "react-bootstrap";


export default function Register(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [passwordOne, setPasswordOne] = useState("");
	const [passwordTwo, setPasswordTwo] = useState("");

	function registerUser(e){
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers:{
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})
			}
			else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: passwordOne
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						setFirstName("");
						setLastName("");
						setEmail("");
						setPasswordOne("");
						setPasswordTwo("");

						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome to Ahl Lightning!"
						})

						//navigate("/login");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})

	}

	const [isActive, setIsActive] = useState(false);


	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== ""  && passwordOne !== "" && passwordTwo !== "") && (passwordOne === passwordTwo)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[firstName, lastName, email, passwordOne, passwordTwo])


	return(
	
		<>
			
		<Row className="justify-content-center">
			 <Col lg="6">
			 <Card className="text-center" border="primary" >
			  <Form onSubmit = {(e) => registerUser(e)}>
      <Card.Header variant="success"><h1>Register</h1>	</Card.Header>
      <Card.Body>
       
      

				<Form.Group className="mb-3" controlId="firstName">
				  <Form.Label>First Name</Form.Label>
				  <Form.Control type="text" placeholder="First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="lastName">
				  <Form.Label>Last Name</Form.Label>
				  <Form.Control type="text" placeholder="Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
				</Form.Group>


			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="passwordOne">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={passwordOne} onChange={e => setPasswordOne(e.target.value)}/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="passwordTwo">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control type="password" placeholder="Verify Password" value={passwordTwo} onChange={e => setPasswordTwo(e.target.value)}/>
			      </Form.Group>
		      
		      
		   
      </Card.Body>
      <Card.Footer className="text-muted">{
		      	isActive
		      	?
		      		<Button variant="primary" type="submit" id="submitBtn">
		      		  Submit
		      		</Button>
		      	:
		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
		      		  Submit
		      		</Button>
		      }</Card.Footer>
       </Form>
    </Card>
					

			</Col>
		</Row>


			
		
		</>
	)
}