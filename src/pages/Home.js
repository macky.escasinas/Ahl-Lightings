

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container'
  import {useEffect, useState} from "react";
import React from 'react';
  import ProductHome from "../components/ProductHome";
import { Card,Image,Table,Col,Row,ButtonGroup,Form,InputGroup} from "react-bootstrap";
import { Link } from "react-router-dom";
//import Jumbotron from 'react-bootstrap/Jumbotron'

import { MDBContainer,MDBBtn  } from 'mdb-react-ui-kit';
export default function Home(){

const myStyle={
        backgroundImage: 
 "./banner.jpg')",
        height:'100vh',
        marginTop:'-70px',
        fontSize:'50px',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
    };

	const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1
        }}
    />
);

    const [products, setProduct] = useState([]);
	useEffect(() =>{


        fetch(`${process.env.REACT_APP_API_URL}/products/viewall-active-product`)
        .then(res => res.json())
        .then(data => {
            setProduct(data.map(product =>{

                return(
                    <ProductHome key={product._id} productProp={product} />
                )
            }))
        })
    },[])
	return(

		<>
<img src="./banner.jpg" className="background-image"/>


{/*<Jumbotron>
  <h1>Hello, world!</h1>
  <p>
    This is a simple hero unit, a simple jumbotron-style component for calling
    extra attention to featured content or information.
  </p>
  <p>
    <Button bsStyle="primary">Learn more</Button>
  </p>
</Jumbotron>*/}



       
 <MDBContainer><div className="d-flex flex-column">
        <div className="p-2"><Container >
       <Container className="tag "><h1><b>HELLO, WELCOME TO AL LIGHTNINGS</b></h1>
        <ColoredLine color="white" />
        <p>
            <MDBBtn>Products</MDBBtn>
          
        </p>
       </Container></Container></div>
      </div>
</MDBContainer>
<br/><br/>
<br/>
<br/><br/>
<br/>
<br/><br/>
<br/>
<br/>
<br/>

<MDBContainer>
 <div className="d-flex p-2"></div>
</MDBContainer>

<MDBContainer>
  <div className='text-center'>
        <h1 className='mt-5'>AL LIGHTNINGS</h1>
      {/*  <h4 className='mb-3'>Sub By Category</h4>
     */}

      </div>
</MDBContainer>
<MDBContainer>
 <div className="d-flex flex-row">

        <div className="p-2"><img src='./home-furnishing-lights.png' className='img-fluid' alt='...' /></div>
        <div className="p-2"><img src='./indoor-lights.png' className='img-fluid' alt='...' /></div>
        <div className="p-2"><img src='./landscape-lights.png' className='img-fluid' alt='...' /></div>
         <div className="p-2"><img src='./light-bulbs-tubes.png' className='img-fluid' alt='...' /></div>
          <div className="p-2"><img src='./outdoor-lights.png' className='img-fluid' alt='...' /></div>
      </div>


     
</MDBContainer>


 
<MDBContainer>
<Row>{products}</Row>
{/*<div className="d-flex flex-lg-row flex-md-column flex-sm-column">*/}

{/*</div>*/}
</MDBContainer>  


   




       


{/*<Container className="home-banner">
    
        

      </Container>*/}
        
		  
		</>
	)
}