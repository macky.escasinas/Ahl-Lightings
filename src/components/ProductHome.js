import { useContext } from "react";
import { Link } from "react-router-dom";
import { Card, Button} from "react-bootstrap";
import Image from "react-bootstrap/Image";

import {FaCartPlus} from "react-icons/fa";
import Col from 'react-bootstrap/Col';

import Swal from "sweetalert2";
import UserContext from "../UserContext";

//Deconstruct the "courseProp" form the "props" object to shorten syntax.
export default function ProductHome({productProp}){
	  	const {user} = useContext(UserContext);
let productLists=[];
	
	const {_id, name, description, price, stock,productImage} = productProp;
	const imagePath=`${process.env.REACT_APP_API_URL}/${productImage}`;
 const addThisToCart = (productId) =>{


 		const list=JSON.parse(localStorage.getItem('productList'));
			
		if(list!=null){
			productLists=list;
			if(productLists.indexOf(productId)===-1){
				productLists.push(productId);
				localStorage.setItem(productId, 1);
				Swal.fire({
                               title: "Product Added",
                            icon: "success",
                             text: "You can change quantity in Cart"
                  })

			}else{
				 Swal.fire({
                               title: "Product Aldeady Added",
                            icon: "error",
                             text: "You can change quantity in Cart"
                  })
			}
			localStorage.setItem('productList', JSON.stringify(productLists));
			

		

		}else{
			
			productLists.push(productId);
			localStorage.setItem('productList', JSON.stringify(productLists));	
			localStorage.setItem(productId, 1);	
			Swal.fire({
                               title: "Product Added",
                            icon: "success",
                             text: "You can change quantity in Cart"
                  })


		}

	
 }


	return (
		<>

	
      <Col lg="4">
      <Card className="p-3 my-3 text-center" variant="success" border="primary">
      <Card.Header><h4>{name}</h4></Card.Header>
      <Card.Body>
     
        <Image src={`${imagePath}`} roundedCircle className="justify-content-center"   width={250} height={250}/>
           <Card.Title >Description: </Card.Title>
        <Card.Text>
          {description}
        </Card.Text>
         <Button as={Link} to={`/product/${_id}`} variant="outline-primary">Discover</Button>
      </Card.Body>
  
    </Card>

		
	   	 </Col>
		</>
	)
}
